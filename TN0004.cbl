       IDENTIFICATION DIVISION.
       PROGRAM-ID.  TR0004.
      *****************************************************************
       ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
           SELECT 000-INPUT-FILE ASSIGN
               TO "D:/Intern by NTT data/Test/new-tr0004/FILE-INPUT.DAT"
               ORGANIZATION IS LINE SEQUENTIAL
               FILE STATUS IS WS-100-FILE-STATUS.

           SELECT 100-OUTPUT-FILE ASSIGN
               TO "D:/Intern by NTT data/Test/new-tr0004/CR04.txt"
               ORGANIZATION IS LINE SEQUENTIAL
               FILE STATUS IS WS-200-FILE-STATUS.

           SELECT 200-OUTPUT-FILE ASSIGN
               TO "D:/Intern by NTT data/Test/new-tr0004/CR04J.txt"
               ORGANIZATION IS LINE SEQUENTIAL
               FILE STATUS IS WS-300-FILE-STATUS.

           SELECT 300-OUTPUT-FILE ASSIGN
               TO "D:/Intern by NTT data/Test/new-tr0004/CR04M.txt"
               ORGANIZATION IS LINE SEQUENTIAL
               FILE STATUS IS WS-400-FILE-STATUS.

           SELECT 400-OUTPUT-FILE ASSIGN
               TO "D:/Intern by NTT data/Test/new-tr0004/CR04V.txt"
               ORGANIZATION IS LINE SEQUENTIAL
               FILE STATUS IS WS-500-FILE-STATUS.

      *****************************************************************
       DATA DIVISION.
       FILE SECTION.
      ******************************************************************
      *INPUT
       FD 000-INPUT-FILE.
       01 000-INPUT-RECORD.
           05 000-CARD-NUM                 PIC X(16).
           05 FILLER                       PIC X VALUE SPACE.
           05 000-AMOUNT                   PIC 9(7)V99.
           05 FILLER                       PIC X VALUE SPACE.
           05 000-MDR                      PIC 9(6)V99.
           05 FILLER                       PIC X VALUE SPACE.
           05 000-VAT                      PIC 9(5)V99.
           05 FILLER                       PIC X VALUE SPACE.
           05 000-CARD-TYPE                PIC X(11).
      ******************************************************************
      *ALL FILE
       FD 100-OUTPUT-FILE.
       01 100-OUTPUT-RECORD                PIC X(133).
      ******************************************************************
      *JBL FILE
       FD 200-OUTPUT-FILE.
       01 200-OUTPUT-RECORD                PIC X(133).
      ******************************************************************
      *MASTER CARD
       FD 300-OUTPUT-FILE.
       01 300-OUTPUT-RECORD                PIC X(133).
      ******************************************************************
      *VISA
       FD 400-OUTPUT-FILE.
       01 400-OUTPUT-RECORD                PIC X(133).

      *****************************************************************
       WORKING-STORAGE SECTION.
       COPY "D:/Intern by NTT data/Test/new-tr0004/COPYBOOK.TXT".

       01 FORMATE-DATE-TIME.
           05 FM-DATE                      PIC X(12).
           05 FM-TIME                      PIC X(12).
       01 WS-CALCULATION.
           05 WS-RECORD-COUNT.
               10 WS-000-REC-COUNT         PIC S9(5) VALUE ZERO.
               10 WS-100-REC-COUNT         PIC S9(5) VALUE ZERO.
               10 WS-200-REC-COUNT         PIC S9(5) VALUE ZERO.
               10 WS-300-REC-COUNT         PIC S9(5) VALUE ZERO.
               10 WS-400-REC-COUNT         PIC S9(5) VALUE ZERO.

           05 WS-LINE-COUNT-FILE1          PIC S9(3) VALUE +55.
           05 WS-LINE-COUNT-FILE2          PIC S9(3) VALUE +55.
           05 WS-LINE-COUNT-FILE3          PIC S9(3) VALUE +55.
           05 WS-LINE-COUNT-FILE4          PIC S9(3) VALUE +55.

           05 WS-LINE-MAX                  PIC S9(3) VALUE +55.
           05 WS-LINE-HEADER-BEGIN         PIC S9(3) VALUE +5.

           05 WS-SPACE-LINE                PIC X     VALUE SPACE.
       01 WS-ALL-FILE.
           05 WS-100-OUTPUT-ALL-FILE.
              10 WS-100-CARD-NUMBER        PIC X(19).
              10 FILLER                    PIC X(8).
              10 WS-100-AMOUNT             PIC Z,ZZZ,ZZ9.99.
              10 FILLER                    PIC X(15).
              10 WS-100-MDR                PIC ZZZ,ZZ9.99.
              10 FILLER                    PIC X(14).
              10 WS-100-VAT                PIC ZZ,ZZ9.99.
              10 FILLER                    PIC X(22).
              10 WS-100-CARD-TYPE          PIC X(11).
           05 WS-200-OUTPUT-VISA-FILE.
              10 WS-200-CARD-NUMBER        PIC X(24).
              10 FILLER                    PIC X(17).
              10 WS-200-AMOUNT             PIC Z,ZZZ,ZZ9.99.
              10 FILLER                    PIC X(24).
              10 WS-200-MDR                PIC ZZZ,ZZ9.99.
              10 FILLER                    PIC X(24).
              10 WS-200-VAT                PIC ZZ,ZZ9.99.
           05 WS-300-OUTPUT-MASTER-CARD-FILE.
              10 WS-300-CARD-NUMBER        PIC X(24).
              10 FILLER                    PIC X(18).
              10 WS-300-AMOUNT             PIC Z,ZZ,ZZ9.99.
              10 FILLER                    PIC X(24).
              10 WS-300-MDR                PIC ZZZ,ZZ9.99.
              10 FILLER                    PIC X(24).
              10 WS-300-VAT                PIC ZZ,ZZ9.99.
           05 WS-400-OUTPUT-JBL-FILE.
              10 WS-400-CARD-NUMBER        PIC X(24).
              10 FILLER                    PIC X(18).
              10 WS-400-AMOUNT             PIC Z,ZZ,ZZ9.99.
              10 FILLER                    PIC X(24).
              10 WS-400-MDR                PIC ZZZ,ZZ9.99.
              10 FILLER                    PIC X(24).
              10 WS-400-VAT                PIC ZZ,ZZ9.99.
       01 WS-FORMAT-DIGIT.
           05 DIGIT-1                      PIC X(4).
           05 FILLER                       PIC X VALUE "-".
           05 DIGIT-2                      PIC X(4).
           05 FILLER                       PIC X VALUE "-".
           05 DIGIT-3                      PIC X(4).
           05 FILLER                       PIC X VALUE "-".
           05 DIGIT-4                      PIC X(4).
       01 WS-END-DETAIL.
           05 WS-TOTAL_VISA_TXN            PIC 999.
           05 WS-TOTAL_VISA_AMT            PIC 9(10)V99.
           05 WS-TOTAL_MASTER_TXN          PIC 999.
           05 WS-TOTAL_MASTER_AMT          PIC 9(10)V99.
           05 WS-TOTAL_JCB_TXN             PIC 999.
           05 WS-TOTAL_JCB_AMT             PIC 9(10)V99.
           05 WS-GRAND_TXN                 PIC 999.
           05 WS-GRAND_AMT                 PIC 9(10)V99.

       01 WS-OUTPUT-VISA-TXN.
           05 FILLER          PIC X(23) VALUE "Total Visa Txn       = ".
           05 WS-OUTPUT-TOTAL-VISA-TXN     PIC 999.
       01 WS-OUTPUT-VISA-AMT.
           05 FILLER          PIC X(23) VALUE "Total Visa Amt       = ".
           05 WS-OUTPUT-TOTAL-VISA-AMT     PIC ZZ,ZZZ,ZZ9.99.


       01 WS-OUTPUT-MASTER-TXN.
           05 FILLER          PIC X(23) VALUE "Total Mastercard Txn = ".
           05 WS-OUTPUT-TOTAL_MASTER_TXN   PIC 999.
       01 WS-OUTPUT-MASTER-AMT.
           05 FILLER          PIC X(23) VALUE "Total Mastercard Amt = ".
           05 WS-OUTPUT-TOTAL_MASTER_AMT   PIC ZZ,ZZZ,ZZ9.99.

       01 WS-OUTPUT-JCB-TXN.
           05 FILLER          PIC X(23) VALUE "Total JCB Txn        = ".
           05 WS-OUTPUT-TOTAL_JCB_TXN       PIC 999.
       01 WS-OUTPUT-JCB-AMT.
           05 FILLER          PIC X(23) VALUE "Total JCB Amt        = ".
           05 WS-OUTPUT-TOTAL_JCB_AMT       PIC ZZ,ZZZ,ZZ9.99.
       01 WS-OUTPUT-TOTAL-TXN.
           05 FILLER          PIC X(23) VALUE "Grand Total Txn      = ".
           05 WS-OUTPUT-GRAND_TXN           PIC 999.
       01 WS-OUTPUT-TOTAL-AMT.
           05 FILLER          PIC X(23) VALUE "Grand Total Amt      = ".
           05 WS-OUTPUT-GRAND_AMT           PIC ZZ,ZZZ,ZZ9.99.
       01 WS-CNT                            PIC 99 VALUE 1.
       PROCEDURE DIVISION.
       0000-MAIN.
           PERFORM 1000-INITAIL    THRU 1000-EXIT.
           PERFORM 2000-PROCESS    THRU 2000-EXIT
               UNTIL FILE-AT-END OF WS-100-FILE-STATUS.
           PERFORM 3000-END        THRU 3000-EXIT.
           STOP RUN.
       0000-EXIT.
           EXIT.
       1000-INITAIL.
           ACCEPT FM-DATE FROM DATE.
           ACCEPT FM-TIME FROM TIME.

           MOVE FM-DATE(5:2)       TO DAY1     DAY2    DAY3    DAY4.
           MOVE FM-DATE(3:2)       TO MONTH1   MONTH2  MONTH3  MONTH4.
           MOVE FM-DATE(1:2)       TO YEAR1    YEAR2   YEAR3   YEAR4.

           MOVE FM-TIME(1:2)       TO HOUR1-2  HOUR2-2 HOUR3-2 HOUR4-2.
           MOVE FM-TIME(3:2)       TO MIN1-2   MIN2-2  MIN3-2  MIN4-2.
           MOVE FM-TIME(5:2)       TO SEC1-2   SEC2-2  SEC3-2  SEC4-2.

           OPEN INPUT 000-INPUT-FILE
               OUTPUT 100-OUTPUT-FILE
                      200-OUTPUT-FILE
                      300-OUTPUT-FILE
                      400-OUTPUT-FILE.
           PERFORM 9100-CHK-FILE-100 THRU 9500-CHK-FILE-500.
           PERFORM 8000-READ-FILE THRU 8000-EXIT.
       1000-EXIT.
           EXIT.
       2000-PROCESS.
           PERFORM 5000-CHK-HEADER         THRU 5000-EXIT.
           PERFORM 6000-MOVE-OUTPUT        THRU 6000-EXIT.

       2000-EXIT.
           EXIT.
       3000-END.
           MOVE WS-TOTAL_VISA_TXN      TO WS-OUTPUT-TOTAL-VISA-TXN.
           MOVE WS-TOTAL_VISA_AMT      TO WS-OUTPUT-TOTAL-VISA-AMT.
           MOVE WS-TOTAL_MASTER_TXN    TO WS-OUTPUT-TOTAL_MASTER_TXN.
           MOVE WS-TOTAL_MASTER_AMT    TO WS-OUTPUT-TOTAL_MASTER_AMT.
           MOVE WS-TOTAL_JCB_TXN       TO WS-OUTPUT-TOTAL_JCB_TXN.
           MOVE WS-TOTAL_JCB_AMT       TO WS-OUTPUT-TOTAL_JCB_AMT.
           MOVE WS-GRAND_TXN           TO WS-OUTPUT-GRAND_TXN.
           MOVE WS-GRAND_AMT           TO WS-OUTPUT-GRAND_AMT.

           MOVE WS-SPACE-LINE TO 400-OUTPUT-RECORD.
           PERFORM 7000-WRITE-FILE-VISA THRU 7000-EXIT.
           MOVE WS-OUTPUT-VISA-TXN TO 400-OUTPUT-RECORD.
           PERFORM 7000-WRITE-FILE-VISA THRU 7000-EXIT.
           MOVE WS-OUTPUT-VISA-AMT TO 400-OUTPUT-RECORD.
           PERFORM 7000-WRITE-FILE-VISA THRU 7000-EXIT.

           MOVE WS-SPACE-LINE TO 100-OUTPUT-RECORD.
           PERFORM 7300-WRITE-FILE-ALL THRU 7300-EXIT.
           MOVE WS-OUTPUT-VISA-TXN TO 100-OUTPUT-RECORD.
           PERFORM 7300-WRITE-FILE-ALL THRU 7300-EXIT.
           MOVE WS-OUTPUT-VISA-AMT TO 100-OUTPUT-RECORD.
           PERFORM 7300-WRITE-FILE-ALL THRU 7300-EXIT.


           MOVE WS-SPACE-LINE TO 300-OUTPUT-RECORD.
           PERFORM 7100-WRITE-FILE-MASTER-CARD THRU 7100-EXIT.
           MOVE WS-OUTPUT-MASTER-TXN TO 300-OUTPUT-RECORD.
           PERFORM 7100-WRITE-FILE-MASTER-CARD THRU 7100-EXIT.
           MOVE WS-OUTPUT-MASTER-AMT TO 300-OUTPUT-RECORD.
           PERFORM 7100-WRITE-FILE-MASTER-CARD THRU 7100-EXIT.

           MOVE WS-SPACE-LINE TO 100-OUTPUT-RECORD.
           PERFORM 7300-WRITE-FILE-ALL THRU 7300-EXIT.
           MOVE WS-OUTPUT-MASTER-TXN TO 100-OUTPUT-RECORD.
           PERFORM 7300-WRITE-FILE-ALL THRU 7300-EXIT.
           MOVE WS-OUTPUT-MASTER-AMT TO 100-OUTPUT-RECORD.
           PERFORM 7300-WRITE-FILE-ALL THRU 7300-EXIT.

           MOVE WS-SPACE-LINE TO 200-OUTPUT-RECORD.
           PERFORM 7200-WRITE-FILE-JCB THRU 7200-EXIT.
           MOVE WS-OUTPUT-JCB-TXN TO 200-OUTPUT-RECORD.
           PERFORM 7200-WRITE-FILE-JCB THRU 7200-EXIT.
           MOVE WS-OUTPUT-JCB-AMT TO 200-OUTPUT-RECORD.
           PERFORM 7200-WRITE-FILE-JCB THRU 7200-EXIT.
           MOVE WS-SPACE-LINE TO 100-OUTPUT-RECORD.
           PERFORM 7300-WRITE-FILE-ALL THRU 7300-EXIT.
           MOVE WS-OUTPUT-JCB-TXN TO 100-OUTPUT-RECORD.
           PERFORM 7300-WRITE-FILE-ALL THRU 7300-EXIT.
           MOVE WS-OUTPUT-JCB-AMT TO 100-OUTPUT-RECORD.
           PERFORM 7300-WRITE-FILE-ALL THRU 7300-EXIT.

           MOVE WS-SPACE-LINE TO 100-OUTPUT-RECORD.
           PERFORM 7300-WRITE-FILE-ALL THRU 7300-EXIT.
           MOVE WS-OUTPUT-TOTAL-TXN TO 100-OUTPUT-RECORD.
           PERFORM 7300-WRITE-FILE-ALL THRU 7300-EXIT.
           MOVE WS-OUTPUT-TOTAL-AMT TO 100-OUTPUT-RECORD.
           PERFORM 7300-WRITE-FILE-ALL THRU 7300-EXIT.

           CLOSE 000-INPUT-FILE
                 100-OUTPUT-FILE
                 200-OUTPUT-FILE
                 300-OUTPUT-FILE
                 400-OUTPUT-FILE.
           PERFORM 9100-CHK-FILE-100 THRU 9500-CHK-FILE-500.
       3000-EXIT.
           EXIT.
       5000-CHK-HEADER.
           ADD +1 TO WS-LINE-COUNT-FILE1.
           ADD +1 TO WS-LINE-COUNT-FILE2.
           ADD +1 TO WS-LINE-COUNT-FILE3.
           ADD +1 TO WS-LINE-COUNT-FILE4.

           IF WS-LINE-COUNT-FILE1 >= WS-LINE-MAX THEN
      *WRITE HEADER VISA
      ******************************************************************
           MOVE WS-HEADER-REPORT-LINE-1-FILE-1 TO 400-OUTPUT-RECORD
           PERFORM 7000-WRITE-FILE-VISA        THRU 7000-EXIT
           MOVE WS-HEADER-REPORT-LINE-2-FILE-1 TO 400-OUTPUT-RECORD
           PERFORM 7000-WRITE-FILE-VISA        THRU 7000-EXIT
           MOVE WS-SPACE-LINE                  TO 400-OUTPUT-RECORD
           PERFORM 7000-WRITE-FILE-VISA        THRU 7000-EXIT
           MOVE REPORT-OUTPUT-NORMALL TO 400-OUTPUT-RECORD
           PERFORM 7000-WRITE-FILE-VISA THRU 7000-EXIT

           MOVE WS-LINE-HEADER-BEGIN TO WS-LINE-COUNT-FILE1
           END-IF.

           IF WS-LINE-COUNT-FILE2 >= WS-LINE-MAX THEN
      *WRITE HEADER MASTER CARD
      ******************************************************************
           MOVE WS-HEADER-REPORT-LINE-1-FILE-2 TO 300-OUTPUT-RECORD
           PERFORM 7100-WRITE-FILE-MASTER-CARD THRU 7100-EXIT
           MOVE WS-HEADER-REPORT-LINE-2-FILE-2 TO 300-OUTPUT-RECORD
           PERFORM 7100-WRITE-FILE-MASTER-CARD THRU 7100-EXIT
           MOVE WS-SPACE-LINE                  TO 300-OUTPUT-RECORD
           PERFORM 7100-WRITE-FILE-MASTER-CARD THRU 7100-EXIT
           MOVE REPORT-OUTPUT-NORMALL TO 300-OUTPUT-RECORD
           PERFORM 7100-WRITE-FILE-MASTER-CARD THRU 7100-EXIT

           MOVE WS-LINE-HEADER-BEGIN TO WS-LINE-COUNT-FILE2
           END-IF.

           IF WS-LINE-COUNT-FILE3 >= WS-LINE-MAX THEN
      *WRITE HEADER JCB
      ******************************************************************
           MOVE WS-HEADER-REPORT-LINE-1-FILE-3 TO 200-OUTPUT-RECORD
           PERFORM 7200-WRITE-FILE-JCB         THRU 7200-EXIT
           MOVE WS-HEADER-REPORT-LINE-2-FILE-3 TO 200-OUTPUT-RECORD
           PERFORM 7200-WRITE-FILE-JCB         THRU 7200-EXIT
           MOVE WS-SPACE-LINE                  TO 200-OUTPUT-RECORD
           PERFORM 7200-WRITE-FILE-JCB         THRU 7200-EXIT
           MOVE REPORT-OUTPUT-NORMALL TO 200-OUTPUT-RECORD
           PERFORM 7200-WRITE-FILE-JCB         THRU 7200-EXIT

           MOVE WS-LINE-HEADER-BEGIN TO WS-LINE-COUNT-FILE3
           END-IF.

           IF WS-LINE-COUNT-FILE4 >= WS-LINE-MAX THEN
      *WRITE HEADER ALL FILE
      ******************************************************************
           MOVE WS-HEADER-REPORT-LINE-1-FILE-4 TO 100-OUTPUT-RECORD
           PERFORM 7300-WRITE-FILE-ALL         THRU 7300-EXIT
           MOVE WS-HEADER-REPORT-LINE-2-FILE-4 TO 100-OUTPUT-RECORD
           PERFORM 7300-WRITE-FILE-ALL         THRU 7300-EXIT
           MOVE WS-SPACE-LINE                  TO 100-OUTPUT-RECORD
           PERFORM 7300-WRITE-FILE-ALL         THRU 7300-EXIT
           MOVE REPORT-OUTPUT-ALLFILE TO 100-OUTPUT-RECORD
           PERFORM 7300-WRITE-FILE-ALL         THRU 7300-EXIT

           MOVE WS-LINE-HEADER-BEGIN TO WS-LINE-COUNT-FILE4
           END-IF.
      ******************************************************************

       5000-EXIT.
           EXIT.
       6000-MOVE-OUTPUT.
           PERFORM 9600-FORMAT-DIGIT.

           IF 000-CARD-TYPE = "Visa"
               MOVE WS-FORMAT-DIGIT            TO WS-200-CARD-NUMBER
               MOVE 000-AMOUNT                 TO WS-200-AMOUNT
               MOVE 000-MDR                    TO WS-200-MDR
               MOVE 000-VAT                    TO WS-200-VAT
               MOVE WS-200-OUTPUT-VISA-FILE    TO 400-OUTPUT-RECORD
               PERFORM 7000-WRITE-FILE-VISA THRU 7000-EXIT
               PERFORM 6100-ALL-FILE

               ADD +1 TO WS-GRAND_TXN
               ADD 000-AMOUNT TO WS-GRAND_AMT
               ADD +1 TO WS-TOTAL_VISA_TXN
               ADD 000-AMOUNT TO WS-TOTAL_VISA_AMT
           END-IF.

           IF 000-CARD-TYPE = "Mastercard"
               MOVE WS-FORMAT-DIGIT            TO WS-300-CARD-NUMBER
               MOVE 000-AMOUNT                 TO WS-300-AMOUNT
               MOVE 000-MDR                    TO WS-300-MDR
               MOVE 000-VAT                    TO WS-300-VAT
               MOVE WS-300-OUTPUT-MASTER-CARD-FILE TO 300-OUTPUT-RECORD
               PERFORM 7100-WRITE-FILE-MASTER-CARD THRU 7100-EXIT
               PERFORM 6100-ALL-FILE

               ADD +1 TO WS-GRAND_TXN
               ADD 000-AMOUNT TO WS-GRAND_AMT
               ADD +1 TO WS-TOTAL_MASTER_TXN
               ADD 000-AMOUNT TO WS-TOTAL_MASTER_AMT
           END-IF.

           IF 000-CARD-TYPE = "JCB"
               MOVE WS-FORMAT-DIGIT            TO WS-400-CARD-NUMBER
               MOVE 000-AMOUNT                 TO WS-400-AMOUNT
               MOVE 000-MDR                    TO WS-400-MDR
               MOVE 000-VAT                    TO WS-400-VAT
               MOVE WS-400-OUTPUT-JBL-FILE TO 200-OUTPUT-RECORD
               PERFORM 7200-WRITE-FILE-JCB THRU 7200-EXIT
               PERFORM 6100-ALL-FILE

               ADD +1 TO WS-GRAND_TXN
               ADD 000-AMOUNT TO WS-GRAND_AMT
               ADD +1 TO WS-TOTAL_JCB_TXN
               ADD 000-AMOUNT TO WS-TOTAL_JCB_AMT
           END-IF.

           PERFORM 8000-READ-FILE THRU 8000-EXIT.
       6000-EXIT.
           EXIT.
       6100-ALL-FILE.
               MOVE WS-FORMAT-DIGIT TO WS-100-CARD-NUMBER.
               MOVE 000-AMOUNT     TO WS-100-AMOUNT.
               MOVE 000-MDR        TO WS-100-MDR.
               MOVE 000-VAT        TO WS-100-VAT.
               MOVE 000-CARD-TYPE TO WS-100-CARD-TYPE.

               MOVE WS-100-OUTPUT-ALL-FILE TO 100-OUTPUT-RECORD.
               PERFORM 7300-WRITE-FILE-ALL THRU 7300-EXIT.

       7000-WRITE-FILE-VISA.
           WRITE 400-OUTPUT-RECORD.
           PERFORM 9500-CHK-FILE-500.
       7000-EXIT.
           EXIT.
       7100-WRITE-FILE-MASTER-CARD.
           WRITE 300-OUTPUT-RECORD.
           PERFORM 9400-CHK-FILE-400.
       7100-EXIT.
           EXIT.
       7200-WRITE-FILE-JCB.
           WRITE 200-OUTPUT-RECORD.
           PERFORM 9300-CHK-FILE-300.
       7200-EXIT.
           EXIT.
       7300-WRITE-FILE-ALL.
           WRITE 100-OUTPUT-RECORD.
           PERFORM 9200-CHK-FILE-200.
       7300-EXIT.
           EXIT.

       8000-READ-FILE.
           READ 000-INPUT-FILE.
           PERFORM 9100-CHK-FILE-100.
       8000-EXIT.
           EXIT.
       9000-ABEND.
           MOVE 12 TO RETURN-CODE.
           GOBACK.
       9000-EXIT.
           EXIT.
       9100-CHK-FILE-100.
           IF FILE-OK OF WS-100-FILE-STATUS OR
               FILE-AT-END OF WS-100-FILE-STATUS
               CONTINUE
           ELSE
              DISPLAY "ABEND MESSAGE 1 FILE = " WS-100-FILE-STATUS
               WS-100-FILE-STATUS
              PERFORM 9000-ABEND THRU 9000-EXIT
           END-IF.
       9200-CHK-FILE-200.
           IF FILE-OK OF WS-200-FILE-STATUS OR
               FILE-AT-END OF WS-200-FILE-STATUS
               CONTINUE
           ELSE
              DISPLAY "ABEND MESSAGE 2 FILE." WS-200-FILE-STATUS
              PERFORM 9000-ABEND THRU 9000-EXIT
           END-IF.
       9300-CHK-FILE-300.
           IF FILE-OK OF WS-300-FILE-STATUS OR
               FILE-AT-END OF WS-300-FILE-STATUS
               CONTINUE
           ELSE
              DISPLAY "ABEND MESSAGE 3 FILE." WS-300-FILE-STATUS
              PERFORM 9000-ABEND THRU 9000-EXIT
           END-IF.
       9400-CHK-FILE-400.
           IF FILE-OK OF WS-400-FILE-STATUS OR
               FILE-AT-END OF WS-400-FILE-STATUS
               CONTINUE
           ELSE
              DISPLAY "ABEND MESSAGE 4 FILE." WS-400-FILE-STATUS
              PERFORM 9000-ABEND THRU 9000-EXIT
           END-IF.
       9500-CHK-FILE-500.
           IF FILE-OK OF WS-500-FILE-STATUS OR
               FILE-AT-END OF WS-500-FILE-STATUS
               CONTINUE
           ELSE
              DISPLAY "ABEND MESSAGE 5 FILE." WS-500-FILE-STATUS
              PERFORM 9000-ABEND THRU 9000-EXIT
           END-IF.
       9600-FORMAT-DIGIT.
           MOVE 000-CARD-NUM(1:4) TO DIGIT-1.
           MOVE 000-CARD-NUM(5:4) TO DIGIT-2.
           MOVE 000-CARD-NUM(9:4) TO DIGIT-3.
           MOVE 000-CARD-NUM(13:4) TO DIGIT-4.
       9700-FORMAT-NUMERIC.

           MOVE WS-SPACE-LINE TO 400-OUTPUT-RECORD.
           PERFORM 7000-WRITE-FILE-VISA THRU 7000-EXIT.

           PERFORM VARYING WS-CNT FROM 1 BY 1 UNTIL WS-CNT > 3
                OR (WS-OUTPUT-TOTAL-VISA-TXN(WS-CNT:1) > ZEROS
                AND WS-OUTPUT-TOTAL-VISA-TXN(WS-CNT:1) IS NUMERIC)

           END-PERFORM.

           EXIT.
